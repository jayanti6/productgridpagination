function ProductPageCreator(domDetails){
  this.allProducts = [];
  this.sideFilters = domDetails.filter;
  this.content = domDetails.content;
  this.footer  = domDetails.footer;
}

ProductPageCreator.prototype.init = function(){
  this.getJsonData();
}

ProductPageCreator.prototype.getJsonData = function(){
  var _this = this;
  $.getJSON("product.json")
    .done(function(data){
      $.each(data, function(key, value){
        _this.createProduct(value);
      });
      _this.allProductsShowing = _this.allProducts;
      _this.selectedValue = _this.allProducts.length;
      _this.createSideFilter();
      _this.showProducts(0, _this.allProductsShowing.length - 1);
    });
};

ProductPageCreator.prototype.createProduct = function(value){
  var product = new Product(value);
  this.allProducts.push(product);
}

ProductPageCreator.prototype.createSideFilter = function(){
  this.createCategoryFilter("brand");
  this.sideFilters.append("<hr>");
  this.createCategoryFilter("color");
  this.sideFilters.append("<hr>");
  this.createAvailableFilter();
  this.sideFilters.append("<hr>");
  this.createSelectBoxForPagination();
}

ProductPageCreator.prototype.createCategoryFilter = function(val){
  var uniqueValuesInCategory = [];
  this.allProducts.forEach(function(element){
    var valueToinsert = (val == "brand") ? element.brand : element.color;
    uniqueValuesInCategory.push(valueToinsert);
  });
  var uniqueValuesInCategory = Array.from(new Set(uniqueValuesInCategory)).sort();

  var _this = this;
  var list = $("<ol></ol>");
  uniqueValuesInCategory.forEach(function(element){
    var colorListItem = $("<li></li>");
    var checkbox = $("<input>");
    checkbox.attr({
      type: "checkbox",
      name: val,
      value: element,
      id: element
    });

    checkbox.on("click", $.proxy(_this.checkboxClick, _this));

    var labelColor = $("<label></label>");
    labelColor.text(element);
    labelColor.attr("for", element);
    colorListItem.append(labelColor, checkbox);
    list.append(colorListItem);
    _this.sideFilters.append(list);
  });
}
ProductPageCreator.prototype.checkboxClick = function(){
  this.allProductsShowing = this.allProducts;
  var _this = this;
  var brandsSelected = [], colorsSelected = [], availablesSelected = [];
  $("input:checked").each(function(){
    var elem = $(this);
    if(elem.attr("name") == "brand"){
      brandsSelected = brandsSelected.concat(_this.allProducts.filter(function(item, index, arr){
       return elem.attr("value") == item.brand;
      }));
    }

    if(elem.attr("name") == "color"){
      colorsSelected = colorsSelected.concat(_this.allProducts.filter(function(item, index, arr){
       return elem.attr("value") == item.color;
      }));
    }

    if(elem.attr("name") == "soldOut"){
      availablesSelected = availablesSelected.concat(_this.allProducts.filter(function(item, index, arr){
       return item.soldOut == "0";
      }));
    }
  });
  var allSelected = [];
  if(brandsSelected.length) allSelected.push(brandsSelected);
  if(colorsSelected.length) allSelected.push(colorsSelected);
  if(availablesSelected.length) allSelected.push(availablesSelected);
  if(allSelected.length) {
    this.allProductsShowing = this.findCommon(allSelected);
  }

  this.createFooter(this.allProductsShowing.length);
  if (this.selectedValue < this.allProductsShowing.length)
    this.showProducts(0, this.selectedValue-1);
  else
    this.showProducts(0, this.allProductsShowing.length-1)
}

ProductPageCreator.prototype.findCommon = function(arr){
  if(arr.length > 1){
    if(arr[1]){
      var common = $.grep(arr[0], function(element) {
        return $.inArray(element, arr[1] ) !== -1;
      });
    }
    if(arr[2]){
      var common = $.grep(common, function(element) {
        return $.inArray(element, arr[2] ) !== -1;
      });
    }
  return common;
  }
  else{
    return arr[0];
  }
}

ProductPageCreator.prototype.createAvailableFilter = function(){
  var _this = this;
  var availableLabel = $("<label></label>");
  availableLabel.text("Available");
  availableLabel.attr("for", "available").css("margin-left", "15px");

  var availableCheckbox = $("<input>");
  availableCheckbox.attr({
    type: "checkbox",
    name: "soldOut",
    value: "available",
    id:"available"
  });

  var availableList = $("<p></p>");
  availableList.append(availableLabel, availableCheckbox);
  availableCheckbox.on("click", $.proxy(_this.checkboxClick, _this));
  this.sideFilters.append(availableList);
};

ProductPageCreator.prototype.createSelectBoxForPagination = function(){
  this.selectBox = $("<select></select>");
  this.selectBox.css({
    width: "100px",
    "margin-left": "15px"
  })
  var optionInitial = $("<option></option>");
  optionInitial.attr("value", "20");
  optionInitial.text("all");

  var option3 = $("<option></option>");
  option3.attr("value", "3");
  option3.text("3");

  var option6 = $("<option></option>");
  option6.attr("value", "6");
  option6.text("6");

  var option9 = $("<option></option>");
  option9.attr("value", "9");
  option9.text("9");

  this.selectBox.append(optionInitial, option3, option6, option9);

  var _this = this;
  this.selectBox.on("change", function(){
    _this.selectedValue = $(this).val();
    _this.showProducts(0, _this.selectedValue-1);
    _this.createFooter();
  });

  this.sideFilters.append(this.selectBox);
}


ProductPageCreator.prototype.createFooter = function(){
  var max = this.allProductsShowing.length;
  var numberOfButtonsToCreate = Math.ceil(max/this.selectedValue);
  var start, end;
  this.footer.text("");
  for(var i = 0; i < numberOfButtonsToCreate; i++){
    var pageNumber = $("<span></span>");
    pageNumber.text(i+1);
    pageNumber.css("margin-right", "10px");
    var _this = this;
    (function(buttonNumber){
      pageNumber.on("click", function(){
        $(this).css("color", "red").siblings().css("color", "white");

        start = buttonNumber * +_this.selectedValue;
        end = start + +_this.selectedValue - 1;
        _this.showProducts(start, end);
      });
    })(i);
    this.footer.append(pageNumber);
  }
}

ProductPageCreator.prototype.showProducts = function(from, to){
  this.content.empty();
  var _this = this;
  if (to > this.allProductsShowing.length-1) { to = this.allProductsShowing.length-1 }
    for(var i = from; i <= to; i++){
      var element = this.allProductsShowing[i];
      var productDiv = $("<div></div>");
      productDiv.css({
        float:"left",
        border: "2px solid black",
        width: "100px",
        height: "100px",
        margin: "20px",
        padding: "10px",
        position: "relative"
      });

      var productImg = $("<img>");
      productImg.attr({
        "src": "images/"+ element.url
      }).css({
        position: "absolute",
        width: "100px",
        height: "80px",
        top:"20px"
      });

  productDiv.append(productImg);
  _this.content.append(productDiv);
  }
}

function Product(value){
  this.brand = value.brand;
  this.color = value.color;
  this.name = value.name;
  this.soldOut = value.sold_out;
  this.url = value.url;
}
$(document).ready(function(){
  var domDetails = {
    content: $("#content"),
    filter: $("#filters"),
    footer: $("#footer")
  }
  var productPageCreator = new ProductPageCreator(domDetails);
  productPageCreator.init();
})
